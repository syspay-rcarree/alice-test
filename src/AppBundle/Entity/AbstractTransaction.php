<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Entity
 * @ORM\Table(name="transaction")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string", columnDefinition="ENUM('PAYMENT', 'REFUND', 'CHARGEBACK')")
 * @ORM\DiscriminatorMap({
 *       "PAYMENT"     = "PaymentTransaction"
 * })
 */
abstract class AbstractTransaction
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('OPEN', 'PENDING')")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $commission;

    /**
     * @ORM\Column(type="integer")
     */
    protected $amount;

    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $currency;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AbstractPaymentMethod", inversedBy="transactions")
     * @ORM\JoinColumn(name="processor_id", referencedColumnName="id", nullable=false)
     */
    protected $paymentMethod;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="OfferPayment", mappedBy="transaction")
     */
    private $offerPayment;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $createdDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $updatedDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return AbstractTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set commission
     *
     * @param string $commission
     *
     * @return AbstractTransaction
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set amount
     *
     * @param int $amount
     *
     * @return AbstractTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return AbstractTransaction
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set paymentMethod
     *
     * @param AbstractPaymentMethod $paymentMethod
     *
     * @return AbstractTransaction
     */
    public function setPaymentMethod(AbstractPaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return AbstractPaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @return int
     */
    public function getCreatedDate(): int
    {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate(int $createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getOfferPayment() : OfferPayment
    {
        return $this->offerPayment;
    }

    /**
     * @param mixed $offerPayment
     */
    public function setOfferPayment(OfferPayment $offerPayment)
    {
        $this->offerPayment = $offerPayment;
    }

    /**
     * @return int
     */
    public function getUpdatedDate(): int
    {
        return $this->updatedDate;
    }

    /**
     * @param int $updatedDate
     */
    public function setUpdatedDate(int $updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }
}
