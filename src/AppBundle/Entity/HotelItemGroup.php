<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\EntityAbstractItemGroup;

/**
 * HotelItemGroup
 *
 * @ORM\Entity
 */
class HotelItemGroup extends AbstractItemGroup
{
}
