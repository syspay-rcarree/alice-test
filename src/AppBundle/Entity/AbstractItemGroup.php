<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemGroup
 *
 * @ORM\Entity
 * @ORM\Table(name="item_group")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string", columnDefinition="ENUM('HOTEL')")
 * @ORM\DiscriminatorMap({
 *       "HOTEL"           = "HotelItemGroup",
 * })
 */
abstract class AbstractItemGroup
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $externalReference;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\UserItemGroup", mappedBy="itemGroup")
     */
    protected $linkedUsers;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AbstractBooking", mappedBy="itemGroup")
     */
    protected $bookings;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Offer", mappedBy="itemGroup")
     */
    protected $offers;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EmailTemplate", inversedBy="itemGroups")
     * @ORM\JoinColumn(name="email_template_id", referencedColumnName="id", nullable=true)
     */
    protected $emailTemplate;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->itemGroupLevels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set externalReference
     *
     * @param string $externalReference
     *
     * @return AbstractItemGroup
     */
    public function setExternalReference($externalReference)
    {
        $this->externalReference = $externalReference;

        return $this;
    }

    /**
     * Get externalReference
     *
     * @return string
     */
    public function getExternalReference()
    {
        return $this->externalReference;
    }

    /**
     * Set emailTemplate
     *
     * @param \AppBundle\Entity\EmailTemplate $emailTemplate
     *
     * @return AbstractItemGroup
     */
    public function setEmailTemplate(\AppBundle\Entity\EmailTemplate  $emailTemplate)
    {
        $this->emailTemplate = $emailTemplate;

        return $this;
    }

    /**
     * Get emailTemplate
     *
     * @return \AppBundle\Entity\EmailTemplate
     */
    public function getEmailTemplate()
    {
        return $this->emailTemplate;
    }

    /**
     * Get linked users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedUsers()
    {
        return $this->linkedUsers;
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }
}
