<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment Method
 *
 * @ORM\Entity
 * @ORM\Table(name="payment_method")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string", columnDefinition="ENUM('CREDITCARD')")
 * @ORM\DiscriminatorMap({
 *       "CREDITCARD"   = "CreditCard",
 * })
 */
abstract class AbstractPaymentMethod
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $publicDescriptor;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AbstractBooking", mappedBy="paymentMethod")
     */
    protected $bookings;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Offer", mappedBy="paymentMethod")
     */
    protected $offers;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Processor", inversedBy="paymentMethods")
     * @ORM\JoinColumn(name="processor_id", referencedColumnName="id", nullable=false)
     */
    protected $processor;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AbstractTransaction", mappedBy="paymentMethod")
     */
    protected $transactions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Set processor
     *
     * @param \AppBundle\Entity\Processor $processor
     *
     * @return self
     */
    public function setProcessor(\AppBundle\Entity\Processor $processor)
    {
        $this->processor = $processor;

        return $this;
    }

    /**
     * Get processor
     *
     * @return \AppBundle\Entity\Processor
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * Set publicDescriptor
     *
     * @param string $publicDescriptor
     *
     * @return self
     */
    public function setPublicDescriptor($publicDescriptor)
    {
        $this->publicDescriptor = $publicDescriptor;

        return $this;
    }

    /**
     * Get publicDescriptor
     *
     * @return string
     */
    public function getPublicDescriptor()
    {
        return $this->publicDescriptor;
    }
}
