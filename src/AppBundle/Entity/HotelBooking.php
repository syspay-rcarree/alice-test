<?php

namespace AppBundle\Entity;

use AppBundle\Entity\AbstractBooking;
use Doctrine\ORM\Mapping as ORM;

/**
 * HotelBooking
 *
 * @ORM\Entity
 */
class HotelBooking extends AbstractBooking
{

    /**
     * @ORM\Column(type="date")
     */
    private $checkInDate;

    /**
     * @ORM\Column(type="date")
     */
    private $checkOutDate;


    /**
     * Set checkInDate
     *
     * @param \DateTime $checkInDate
     *
     * @return HotelBooking
     */
    public function setCheckInDate($checkInDate)
    {
        $this->checkInDate = $checkInDate;

        return $this;
    }

    /**
     * Get checkInDate
     *
     * @return \DateTime
     */
    public function getCheckInDate()
    {
        return $this->checkInDate;
    }

    /**
     * Set checkOutDate
     *
     * @param \DateTime $checkOutDate
     *
     * @return HotelBooking
     */
    public function setCheckOutDate($checkOutDate)
    {
        $this->checkOutDate = $checkOutDate;

        return $this;
    }

    /**
     * Get checkOutDate
     *
     * @return \DateTime
     */
    public function getCheckOutDate()
    {
        return $this->checkOutDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
