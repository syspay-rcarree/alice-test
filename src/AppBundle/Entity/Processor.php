<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProcessorConfiguration
 *
 * @ORM\Entity
 */
class Processor
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $factory;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\ProcessorConfiguration", mappedBy="processor")
     */
    protected $configurations;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AbstractProcessorTransaction", mappedBy="processor")
     */
    protected $processorTransactions;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AbstractPaymentMethod", mappedBy="processor")
     */
    protected $paymentMethods;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set factory
     *
     * @param string $factory
     *
     * @return Processor
     */
    public function setFactory($factory)
    {
        $this->factory = $factory;

        return $this;
    }

    /**
     * Get factory
     *
     * @return string
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Processor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get configurations
     *
     * @return Collection
     */
    public function getConfigurations()
    {
        return $this->configurations;
    }

    /**
     * Get configurations
     *
     * @return Collection
     */
    public function getProcessorTransactions()
    {
        return $this->processorTransactions;
    }


    /**
     * Get paymentMethods
     *
     * @return Collection
     */
    public function getPaymentMethods()
    {
        return $this->paymentMethods;
    }
}
