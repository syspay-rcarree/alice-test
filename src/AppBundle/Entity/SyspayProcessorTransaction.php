<?php

namespace AppBundle\Entity;

use AppBundle\Entity\AbstractProcessorTransaction;
use Doctrine\ORM\Mapping as ORM;

/**
 * SysPayProcessorTransaction
 *
 * @ORM\Entity
 */
class SyspayProcessorTransaction extends AbstractProcessorTransaction
{

}
