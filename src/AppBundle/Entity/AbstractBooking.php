<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Booking
 *
 * @ORM\Entity
 * @ORM\Table(name="booking", uniqueConstraints={@ORM\UniqueConstraint(name="bookingReferences", columns={"reference", "booking_provider_id"})})
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string", columnDefinition="ENUM('HOTEL')")
 * @ORM\DiscriminatorMap({
 *       "HOTEL" = "HotelBooking",
 * })
 */
abstract class AbstractBooking
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $currency;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('ACTIVE')")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $reference;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AbstractItemGroup", inversedBy="bookings")
     * @ORM\JoinColumn(name="item_group_id", referencedColumnName="id", nullable=false)
     */
    protected $itemGroup;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AbstractPaymentMethod", inversedBy="bookings")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true)
     */
    protected $paymentMethod;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BookingProvider", inversedBy="bookings")
     * @ORM\JoinColumn(name="booking_provider_id", referencedColumnName="id", nullable=false)
     */
    protected $bookingProvider;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return self
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set itemGroup
     *
     * @param \AppBundle\Entity\AbstractItemGroup $itemGroup
     *
     * @return self
     */
    public function setItemGroup(AbstractItemGroup $itemGroup)
    {
        $this->itemGroup = $itemGroup;

        return $this;
    }

    /**
     * Get itemGroup
     *
     * @return \AppBundle\Entity\AbstractItemGroup
     */
    public function getItemGroup()
    {
        return $this->itemGroup;
    }

    /**
     * Set paymentMethod
     *
     * @param \AppBundle\Entity\PaymentMethod $paymentMethod
     *
     * @return self
     */
    public function setPaymentMethod(\AppBundle\Entity\AbstractPaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \AppBundle\Entity\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set BookingProvider
     *
     * @param \AppBundle\Entity\BookingProvider $bookingProvider
     *
     * @return self
     */
    public function setBookingProvider(\AppBundle\Entity\BookingProvider $bookingProvider)
    {
        $this->bookingProvider = $bookingProvider;

        return $this;
    }

    /**
     * Get BookingProvider
     *
     * @return \AppBundle\Entity\BookingProvider
     */
    public function getBookingProvider()
    {
        return $this->bookingProvider;
    }
}
