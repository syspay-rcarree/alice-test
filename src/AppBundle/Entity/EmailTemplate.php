<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailTemplate
 *
 * @ORM\Entity
 */
class EmailTemplate
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('BIDDING')")
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $twig;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AbstractItemGroup", mappedBy="emailTemplate")
     */
    protected $itemGroups;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return EmailTemplate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set twig
     *
     * @param string $twig
     *
     * @return EmailTemplate
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get twig
     *
     * @return string
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Get item groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemGroups()
    {
        return $this->itemGroups;
    }
}
