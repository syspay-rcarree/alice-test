<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProcessorTransaction
 *
 * @ORM\Entity
 * @ORM\Table(name="processor_transaction")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string", columnDefinition="ENUM('SYSPAY','PAYPAL')")
 * @ORM\DiscriminatorMap({"SYSPAY" = "SyspayProcessorTransaction"})
 */
abstract class AbstractProcessorTransaction
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('PENDING')")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $reference;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Processor", inversedBy="processorTransactions")
     * @ORM\JoinColumn(name="processor_id", referencedColumnName="id")
     */
    protected $processor;

    /**
     * @ORM\OneToOne(targetEntity="AbstractTransaction")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    private $transaction;

    /**
     * @var int
     */
    protected $createdDate;

    /**
     * @var int
     */
    protected $updatedDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return string
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return AbstractProcessorTransaction
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set transaction
     *
     * @param \AppBundle\Entity\AbstractTransaction $transaction
     *
     * @return AbstractProcessorTransaction
     */
    public function setTransaction(AbstractTransaction $transaction)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction
     *
     * @return \AppBundle\Entity\AbstractTransaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Set processor
     *
     * @param \AppBundle\Entity\Processor $processor
     *
     * @return AbstractProcessorTransaction
     */
    public function setProcessor(\AppBundle\Entity\Processor $processor)
    {
        $this->processor = $processor;

        return $this;
    }

    /**
     * Get processor
     *
     * @return \AppBundle\Entity\Processor
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @return int
     */
    public function getCreatedDate(): int
    {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate(int $createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return int
     */
    public function getUpdatedDate(): int
    {
        return $this->updatedDate;
    }

    /**
     * @param int $updatedDate
     */
    public function setUpdatedDate(int $updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }


}
