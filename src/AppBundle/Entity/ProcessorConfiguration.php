<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProcessorConfiguration
 *
 * @ORM\Entity
 */
class ProcessorConfiguration
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $config;

    /**
     * @ORM\ManyToOne(targetEntity="Processor", inversedBy="configurations")
     * @ORM\JoinColumn(name="processor_id", referencedColumnName="id", nullable=false)
     */
    protected $processor;

    /**
     * @ORM\ManyToOne(targetEntity="AbstractItemGroup")
     * @ORM\JoinColumn(name="item_group_id", referencedColumnName="id")
     */
    private $itemGroup;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set config
     *
     * @param string $config
     *
     * @return Processor
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set processor
     *
     * @param Processor $processor
     *
     * @return self
     */
    public function setProcessor(Processor $processor)
    {
        $this->processor = $processor;

        return $this;
    }

    /**
     * Get processor
     *
     * @return string
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * Set itemGroup
     *
     * @param AbstractItemGroup $itemGroup
     *
     * @return self
     */
    public function setItemGroup(AbstractItemGroup $itemGroup = null)
    {
        $this->itemGroup = $itemGroup;

        return $this;
    }

    /**
     * Get itemGroup
     *
     * @return AbstractItemGroup
     */
    public function getItemGroup()
    {
        return $this->itemGroup;
    }
}
