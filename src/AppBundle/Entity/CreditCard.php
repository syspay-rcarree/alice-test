<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\AbstractPaymentMethod;

/**
 * CreditCard
 *
 * @ORM\Entity
 */
class CreditCard extends AbstractPaymentMethod
{

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $cardHolderName;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $brand;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $token;

    /**
     * Set cardHolderName
     *
     * @param string $cardHolderName
     *
     * @return CreditCard
     */
    public function setCardHolderName($cardHolderName)
    {
        $this->cardHolderName = $cardHolderName;

        return $this;
    }

    /**
     * Get cardHolderName
     *
     * @return string
     */
    public function getCardHolderName()
    {
        return $this->cardHolderName;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return CreditCard
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return CreditCard
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
