<?php
declare(strict_types = 1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentTransaction
 *
 * @ORM\Entity
 */
class PaymentTransaction extends AbstractTransaction
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $preauth;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $token;

    /**
     * @return bool
     */
    public function isPreauth(): bool
    {
        return $this->preauth;
    }

    /**
     * @param bool $preauth
     */
    public function setPreauth(bool $preauth)
    {
        $this->preauth = $preauth;
    }

    /**
     * @return string
     */
    public function getToken() : string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }


}
