<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 18:41
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\BookingProvider;
use AppBundle\Entity\HotelBooking;
use AppBundle\Entity\Offer;
use AppBundle\Entity\OfferPayment;

class BookingProviderDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $bookingProvider = new BookingProvider();
        $bookingProvider->setProviderKey('provkey');
        $manager->persist($bookingProvider);
        $manager->flush();

        $this->addReference('booking-provider', $bookingProvider);
    }

    public function getOrder()
    {
        return 73;
    }
}