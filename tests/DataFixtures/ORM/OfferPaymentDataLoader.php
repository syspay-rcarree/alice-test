<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 18:41
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\OfferPayment;

class OfferPaymentDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $offerPayment = new OfferPayment();
        $offerPayment->setOffer($this->getReference('offer'));
        $offerPayment->setTransaction($this->getReference('transaction'));
        $manager->persist($offerPayment);
        $manager->flush();

        $this->addReference('admin-user', $offerPayment);
    }

    public function getOrder()
    {
        return 80;
    }
}