<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 18:41
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Offer;
use AppBundle\Entity\OfferPayment;

class OfferDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {$hig=$this->getReference('hotel-item-group');
        $hig->getExternalReference();
        $offer = new Offer();
        $offer->setBooking($this->getReference('booking'));
        $offer->setItemGroup($hig);
        $offer->setAmount(35);
        $offer->setCurrency('EUR');
        $manager->persist($offer);
        $manager->flush();

        $this->addReference('offer', $offer);
    }

    public function getOrder()
    {
        return 75;
    }
}