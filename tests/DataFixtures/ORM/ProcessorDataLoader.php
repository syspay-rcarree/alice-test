<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 13:42
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Processor;

class ProcessorDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $processor = new Processor();
        $processor->setFactory('SyspayProcessor');
        $processor->setName('SyspayProcessor');

        $manager->persist($processor);
        $manager->flush();

        $this->addReference('processor', $processor);
    }
    public function getOrder()
    {
        return 50;
    }
}