<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 12:45
 */

namespace DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\PaymentTransaction;

class TransactionDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $paymentTransaction = new PaymentTransaction();
        $paymentTransaction->setPaymentMethod($this->getReference('credit-card'));
        $paymentTransaction->setStatus('OPEN');
        $paymentTransaction->setAmount(45);
        $paymentTransaction->setCurrency('EUR');
        $paymentTransaction->setPreauth(false);
        $paymentTransaction->setToken('45520');
        $paymentTransaction->setCommission('commission');
        $paymentTransaction->setCreatedDate(time());
        //$paymentTransaction->setOfferPayment()

        $manager->persist($paymentTransaction);
        $manager->flush();

        $this->addReference('transaction', $paymentTransaction);
    }

    public function getOrder()
    {
        return 70;
    }
}