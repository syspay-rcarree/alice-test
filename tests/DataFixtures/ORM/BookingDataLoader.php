<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 18:41
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\HotelBooking;

class BookingDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $hotelBooking = new HotelBooking();
        $hotelBooking->setCurrency('EUR');
        $hotelBooking->setStatus('ACTIVE');
        $hotelBooking->setItemGroup($this->getReference('hotel-item-group'));
        $hotelBooking->setEmail('john.doe@gmail.com');
        $hotelBooking->setReference('reference');
        $hotelBooking->setBookingProvider($this->getReference('booking-provider'));
        $manager->persist($hotelBooking);
        $manager->flush();
        $this->addReference('booking', $hotelBooking);
    }

    public function getOrder()
    {
        return 74;
    }
}