<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 13:28
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\CreditCard;

class PaymentMethodDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $creditCard = new CreditCard();
        $creditCard->setPublicDescriptor('descriptor');
        $creditCard->setCardHolderName('John Doe');
        $creditCard->setBrand('MasterCard');
        $creditCard->setToken('12341234');
        $creditCard->setProcessor($this->getReference('processor'));
        $manager->persist($creditCard);
        $manager->flush();
        $this->addReference('credit-card', $creditCard);
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 60;
    }
}