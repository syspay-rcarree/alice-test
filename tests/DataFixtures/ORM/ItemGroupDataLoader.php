<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/02/2017
 * Time: 18:41
 */

namespace DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\HotelItemGroup;
use AppBundle\Entity\Offer;
use AppBundle\Entity\OfferPayment;

class ItemGroupDataLoader extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $hotelItemGroup = new HotelItemGroup();
        $hotelItemGroup->setExternalReference('IT1234');
        $manager->persist($hotelItemGroup);
        $manager->flush();

        $this->addReference('hotel-item-group', $hotelItemGroup);
    }

    public function getOrder()
    {
        return 72;
    }
}