<?php

namespace Tests\AppBundle\Controller;


use Liip\FunctionalTestBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function testLoadFixtureFiles()
    {

        $this->loadFixtureFiles([
            '../Resources/fixtures/orm/hotel_item_group.yml',
            '../Resources/fixtures/orm/booking.yml',
            '../Resources/fixtures/orm/booking_provider.yml',
            '../Resources/fixtures/orm/credit_card.yml',
            '../Resources/fixtures/orm/email_template.yml',
            '../Resources/fixtures/orm/offer.yml',
            '../Resources/fixtures/orm/offer_payment.yml',
            '../Resources/fixtures/orm/processor.yml',
            '../Resources/fixtures/orm/processor_configuration.yml',
            '../Resources/fixtures/orm/transaction.yml',
        ]);
    }

    public function testLoadFixtures()
    {
        $this->loadFixtures(array(
            'DataFixtures\ORM\BookingDataLoader',
            'DataFixtures\ORM\BookingProviderDataLoader',
            'DataFixtures\ORM\ItemGroupDataLoader',
            'DataFixtures\ORM\OfferDataLoader',
            'DataFixtures\ORM\OfferPaymentDataLoader',
            'DataFixtures\ORM\PaymentMethodDataLoader',
            'DataFixtures\ORM\ProcessorDataLoader',
            'DataFixtures\ORM\TransactionDataLoader'
        ));
    }
}
